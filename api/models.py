from django.db import models


class Book(models.Model):
    title = models.CharField(max_length=60)
    author = models.CharField(max_length=40)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['title']
