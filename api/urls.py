from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from . import views

urlpatterns = [
    path('', views.welcome),
    path('books/', views.Books.as_view()),
    path('books/<int:pk>/', views.Book.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
