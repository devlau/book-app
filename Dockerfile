FROM python:3.5-alpine

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN mkdir -p /backend/static
WORKDIR /backend

# lint
# RUN pip install --upgrade pip
# RUN pip install flake8
# RUN flake8 --ignore=E501,F401 .

# Install the package dependencies (this step is separated
# from copying all the source code to avoid having to
# re-install all python packages defined in requirements.txt
# whenever any source code change is made)
COPY requirements.txt /backend
RUN pip install --no-cache-dir -r requirements.txt
# RUN pip wheel --no-cache-dir --no-deps --wheel-dir /backend/wheels -r requirements.txt

# Copy the source code into the container
COPY . /backend/

EXPOSE 5000

CMD ["/backend/entrypoint.sh"]
# ENTRYPOINT ["/backend/entrypoint.sh"]
