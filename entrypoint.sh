#!/bin/sh

python manage.py makemigrations
python manage.py migrate --no-input
python manage.py collectstatic --no-input --clear
gunicorn projeto.wsgi --bind 0.0.0.0:5000

exec "$@"
